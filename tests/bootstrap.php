<?php
/**
 * bootstrap.php
 *
 * @author: chazer
 * @created: 23.11.15 18:56
 */

require __DIR__ . '/../vendor/autoload.php';

$definitions = [
    'TESTS_MGR_URL',
    'TESTS_MGR_PATH',
    'TESTS_MRG_USER',
    'TESTS_MRG_PASS'
];

foreach ($definitions as $name) {
    defined($name) || define($name, getenv($name));
}
