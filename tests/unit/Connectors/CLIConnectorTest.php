<?php
/**
 * CLIConnectorTest.php
 *
 * @author: chazer
 * @created: 23.11.15 19:07
 */

namespace ISP\Manager\Tests\Connectors;

use ISP\Manager\Connectors\CLIConnector;

class CLIConnectorTest extends \PHPUnit_Framework_TestCase
{
    /** @var CLIConnector */
    private $obj;

    protected function setUp()
    {
        $this->obj = new CLIConnector();
        $this->obj->mgrctl = $this->createExecutable();
        $this->obj->init();
    }

    protected function tearDown()
    {
        unlink($this->obj->mgrctl);
    }

    protected function createExecutable()
    {
        $tempPath = tempnam(sys_get_temp_dir(), 'tmp');
        chmod($tempPath, 0700);
        return $tempPath;
    }

    protected function createNonExecutable()
    {
        $tempPath = tempnam(sys_get_temp_dir(), 'tmp');
        chmod($tempPath, 0600);
        return $tempPath;
    }

    public function testNotExecutableError()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $this->markTestSkipped('Not for Windows OS');
        }

        $c = new CLIConnector();

        // Test executable flag off
        $c->mgrctl = $this->createNonExecutable();
        $this->setExpectedException('Exception');
        $c->init();
        unlink($c->mgrctl);

        // Test executable flag on
        $c->mgrctl = $this->createExecutable();
        $this->assertTrue(chmod($c->mgrctl, 600), "Couldn't change file mode (chmod)");
        $c->init();
        unlink($c->mgrctl);
    }

    public function testNotFoundError()
    {
        // Test no file
        $c = new CLIConnector();
        $c->mgrctl = 'not-a-file';
        $this->setExpectedException('Exception');
        $c->init();
    }

    public function testFunctionName()
    {
        $req = $this->obj->buildRequest('function', []);
        $this->assertStringEndsWith(' function', $req);
    }

    public function testManagerOption()
    {
        $req = $this->obj->buildRequest('none', []);
        $this->assertContains(' -m ispmgr ', $req);

        $req = $this->obj->buildRequest('none', [], ['m' => 'testmgr']);
        $this->assertContains(' -m testmgr ', $req);
    }

    public function testFormatOption()
    {
        $req = $this->obj->buildRequest('none', []);
        $this->assertContains(' -o xml ', $req);

        $req = $this->obj->buildRequest('none', [], ['o' => 'devel']);
        $this->assertContains(' -o devel ', $req);
    }

    public function testBuildRequest()
    {
        $this->obj->defaultOptions = [
            'o' => 'devel',
            'm' => 'testmgr',
        ];

        $req = $this->obj->buildRequest('test.function', ['a' => 1, 'b' => 2, 'c' => 3]);
        $this->assertEquals($this->obj->mgrctl . ' -o devel -m testmgr test.function a=1 b=2 c=3', $req);
    }
}
