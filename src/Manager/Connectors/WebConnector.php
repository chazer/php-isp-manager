<?php
/**
 * WebConnector.php
 *
 * @author: chazer
 * @created: 23.11.15 18:00
 */

namespace ISP\Manager\Connectors;

use Exception;
use ISP\Manager\Interfaces\IConsoleConnector;

class WebConnector implements IConsoleConnector
{
    public $uri = 'http://localhost:1500';

    public $defaultOptions = [
        'm' => 'ispmgr',
        'o' => 'xml',
    ];

    public function init()
    {
        if (!ini_get('allow_url_fopen')) {
            throw new Exception('The setting "allow_url_fopen" must be set to "on"');
        }
    }

    public function buildRequest($func, $params, $options = null)
    {
        if (!$this->uri || (false === parse_url($this->uri)))
            return false;

        $url = rtrim($this->uri, '/');
        $options = is_null($options) ? [] : $options;
        $options = array_merge($this->defaultOptions, $options);
        $params['func'] = $func;

        $url .= '/' . $options['m'];
        unset($options['m']);

        if (isset($options['o'])) {
            $params['out'] = $options['o'];
            unset($options['o']);
        }

        if (!empty($options)) {
            throw new \Exception(sprintf('Unsupported options: %s.', implode(', ', array_keys($options))));
        }

        $query = http_build_query($params);

        $url = $url . '?' . $query;

        return $url;
    }

    /**
     * @param mixed $command
     * @return string
     */
    public function execute($command)
    {
        $handle = fopen($command, 'rb');
        if (false === $handle) {
            exit("Couldn't open URL address");
        }

        $out = '';
        while (!feof($handle)) {
            $out .= fread($handle, 8192);
        }
        fclose($handle);

        return $out;
    }
}
